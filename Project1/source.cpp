#include <iostream>
#include "sort.h"

using namespace std;

int main()
{
	int b[] = { 1,2,4,5,-67,4,32,2,3,+0,0,5,7,2,-5,7,-0,3,2 };

	sort a(b, (sizeof(b) / sizeof(b[0])));
	a.print(); cout << endl;
	a.mergeSort(0, (sizeof(b) / sizeof(b[0]) - 1));
	a.print(); cout << endl;
	char ans = a.verify() ? 'T' : 'F';
	cout << ans << endl;
	a.~sort();
}
