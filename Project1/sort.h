#pragma once

#ifndef sort_h
#define sort_h

const int max_size = 256;

class sort {
private:
	int a[max_size];	// tablica do posortowania
	int size;			// rozmiar tablicy
public:
	sort(int b[] = { 0 }, int size = 1);	// konstruktor
	~sort();
	void print();	// wypisanie tablicy
	void mergeSort(int l, int r);	// metoda sortujaca
	void merge(int l, int m, int r);
	bool verify();	// metoda sprawdzajaca
};

#endif