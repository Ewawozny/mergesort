#include <iostream>

#include "sort.h"

sort::sort(int b[], int s) {
	size = s;
	for (int i = 0; i < size; i++) {
		a[i] = b[i];
	}
}

sort::~sort() {}

void sort::print() {
	for (int i = 0; i < size; i++) {
		std::cout << a[i] << " ";
	}
}

void sort::merge(int l, int m, int r) {
	int left[256];
	int right[256];

	int leftLength = m - l + 1;
	int rightLength = r - m;

	for (int i = 0; i < leftLength; i++)
		left[i] = a[l + i];

	for (int j = 0; j < rightLength; j++)
		right[j] = a[m + 1 + j];

	int i = 0;
	int j = 0;
	int k = l;

	while (i < leftLength && j < rightLength) {
		if (left[i] <= right[j]) {
			a[k] = left[i];
			i++;
		}
		else {
			a[k] = right[j];
			j++;
		}
		k++;
	}

	while (i < leftLength) {
		a[k] = left[i];
		i++;
		k++;
	}

	while (j < rightLength) {
		a[k] = right[j];
		j++;
		k++;
	}
}

void sort::mergeSort(int l, int r) {
	if (l < r) {
		int m = (l + r) / 2;
		mergeSort(l, m);
		mergeSort((m + 1), r);
		merge(l, m, r);
	}
}

bool sort::verify() {
	for (int i = 1; i < size; i++) {
		if (a[i] < a[i - 1]) return false;
	}
	return true;
}